package com.benrski.neuralnetwork.activationfunctions;

public class RectifiedLinearUnit implements IActivationFunction {
    @Override
    public double calculate(double x) {
        if (x <= 0.0) {
            return 0.0;
        } else {
            return x;
        }
    }

    @Override
    public double derivative(double x) {
        if (x <= 0.0) {
            return 0.0;
        } else {
            return 1.0;
        }
    }
}
