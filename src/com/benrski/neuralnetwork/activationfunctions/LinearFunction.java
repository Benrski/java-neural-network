package com.benrski.neuralnetwork.activationfunctions;

public class LinearFunction implements IActivationFunction {
    private double a = 1.0;

    public LinearFunction() {
        this(1.0);
    }

    public LinearFunction(double a) {
        setA(a);
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getA() {
        return a;
    }

    @Override
    public double calculate(double x) {
        return a * x;
    }

    @Override
    public double derivative(double x) {
        return a;
    }
}
