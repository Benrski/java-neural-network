package com.benrski.neuralnetwork.activationfunctions;

public class ParametericRectifiedLinearUnit implements IActivationFunction {
    private double a = 0.01;

    public ParametericRectifiedLinearUnit() {
        this(0.01);
    }

    public ParametericRectifiedLinearUnit(double a) {
        setA(a);
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getA() {
        return a;
    }

    @Override
    public double calculate(double x) {
        if (x < 0.0) {
            return a * x;
        } else {
            return x;
        }
    }

    @Override
    public double derivative(double x) {
        if (x < 0.0) {
            return a;
        } else {
            return 1.0;
        }
    }
}
