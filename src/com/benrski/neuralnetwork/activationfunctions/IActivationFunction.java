package com.benrski.neuralnetwork.activationfunctions;

public interface IActivationFunction {
    double calculate(double x);

    double derivative(double x);
}
