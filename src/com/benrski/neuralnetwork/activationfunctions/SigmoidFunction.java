package com.benrski.neuralnetwork.activationfunctions;

public class SigmoidFunction implements IActivationFunction {
    private double a = 1.0;

    public SigmoidFunction() {
        this(1.0);
    }

    public SigmoidFunction(double a) {
        setA(a);
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getA() {
        return a;
    }

    @Override
    public double calculate(double x) {
        return 1.0 / (1.0 + Math.exp(-a * x));
    }

    @Override
    public double derivative(double x) {
        return calculate(x) * (1.0 - calculate(x));
    }
}
