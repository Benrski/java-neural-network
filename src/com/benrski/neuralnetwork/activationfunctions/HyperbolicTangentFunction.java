package com.benrski.neuralnetwork.activationfunctions;

public class HyperbolicTangentFunction implements IActivationFunction {
    private double a = 1.0;

    public HyperbolicTangentFunction() {
        this(1.0);
    }

    public HyperbolicTangentFunction(double a) {
        setA(a);
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getA() {
        return a;
    }

    @Override
    public double calculate(double x) {
        return (1.0 - Math.exp(-a * x)) / (1.0 + Math.exp(-a * x));
    }

    @Override
    public double derivative(double x) {
        return (1.0) - Math.pow(calculate(x), 2.0);
    }
}
