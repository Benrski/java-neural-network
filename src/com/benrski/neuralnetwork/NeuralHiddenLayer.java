package com.benrski.neuralnetwork;

import com.benrski.neuralnetwork.activationfunctions.IActivationFunction;

public class NeuralHiddenLayer extends NeuralLayer {

    public NeuralHiddenLayer(int neuronsCount, int inputsCount, IActivationFunction activationFunction) {
        this(null, neuronsCount, inputsCount, activationFunction);
    }

    public NeuralHiddenLayer(NeuralNetwork network, int neuronsCount, int inputsCount,
            IActivationFunction activationFunction) {
        super(network, neuronsCount, inputsCount, activationFunction);
    }

    @Override
    public void setPreviousLayer(NeuralLayer layer) {
        // Setting previous layer and checking for previous' next layer
        previousLayer = layer;
        if (layer.getNextLayer() != this) {
            layer.setNextLayer(this);
        }
    }

    @Override
    public void setNextLayer(NeuralLayer layer) {
        // Setting next layer and checking for next's previous layer
        nextLayer = layer;
        if (layer.getPreviousLayer() != this) {
            layer.setPreviousLayer(this);
        }
    }
}
