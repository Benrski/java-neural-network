package com.benrski.neuralnetwork;

import com.benrski.neuralnetwork.activationfunctions.IActivationFunction;
import com.benrski.neuralnetwork.activationfunctions.LinearFunction;
import com.benrski.neuralnetwork.initializers.IInitializer;

public class NeuralNetwork {
    // Input layer
    protected NeuralInputLayer inputLayer;
    // Hidden layers
    protected NeuralHiddenLayer[] hiddenLayers;
    // Output layer
    protected NeuralOutputLayer outputLayer;
    // All layers [INPUT, HIDDENS, OUTPUT]
    protected NeuralLayer[] layers;
    // Inputs
    protected double[] inputs;
    // Outputs
    protected double[] outputs;

    public NeuralNetwork(int inputsCount, int outputsCount, IActivationFunction outputActivationFunction,
            IInitializer initializer) {
        this(inputsCount, new LinearFunction(), outputsCount, outputActivationFunction, initializer);
    }

    public NeuralNetwork(int inputsCount, IActivationFunction inputActivationFunction, int outputsCount,
            IActivationFunction outputActivationFunction, IInitializer initializer) {
        this(inputsCount, inputActivationFunction, new int[0], new IActivationFunction[0], outputsCount,
                outputActivationFunction, initializer);
    }

    public NeuralNetwork(int inputsCount, int hiddenNeuronsCount, IActivationFunction hiddenActivationFunction,
            int outputsCount, IActivationFunction outputActivationFunction, IInitializer initializer) {
        this(inputsCount, new LinearFunction(), hiddenNeuronsCount, hiddenActivationFunction, outputsCount,
                outputActivationFunction, initializer);
    }

    public NeuralNetwork(int inputsCount, IActivationFunction inputActivationFunction, int hiddenNeuronsCount,
            IActivationFunction hiddenActivationFunctions, int outputsCount,
            IActivationFunction outputActivationFunction, IInitializer initializer) {
        this(inputsCount, inputActivationFunction, new int[] { hiddenNeuronsCount },
                new IActivationFunction[] { hiddenActivationFunctions }, outputsCount, outputActivationFunction,
                initializer);
    }

    public NeuralNetwork(int inputsCount, int[] hiddenNeuronsCount, IActivationFunction[] hiddenActivationFunctions,
            int outputsCount, IActivationFunction outputActivationFunction, IInitializer initializer) {
        this(inputsCount, new LinearFunction(), hiddenNeuronsCount, hiddenActivationFunctions, outputsCount,
                outputActivationFunction, initializer);
    }

    public NeuralNetwork(int inputsCount, IActivationFunction inputActivationFunction, int[] hiddenNeuronsCount,
            IActivationFunction[] hiddenActivationFunctions, int outputsCount,
            IActivationFunction outputActivationFunction, IInitializer initializer) {
        inputs = new double[inputsCount];
        outputs = new double[outputsCount];
        // Input layer
        inputLayer = new NeuralInputLayer(this, inputsCount, inputActivationFunction);
        inputLayer.initialize(initializer);
        NeuralLayer previousLayer = inputLayer;
        // Hidden layers
        hiddenLayers = new NeuralHiddenLayer[hiddenNeuronsCount.length];
        for (int i = 0; i < hiddenLayers.length; i++) {
            hiddenLayers[i] = new NeuralHiddenLayer(this, hiddenNeuronsCount[i], previousLayer.getNeuronsCount(),
                    hiddenActivationFunctions[i]);
            hiddenLayers[i].initialize(initializer);
            previousLayer.setNextLayer(hiddenLayers[i]);
            previousLayer = hiddenLayers[i];
        }
        // Output layer
        outputLayer = new NeuralOutputLayer(this, outputsCount, previousLayer.getNeuronsCount(),
                outputActivationFunction);
        outputLayer.initialize(initializer);
        previousLayer.setNextLayer(outputLayer);
        // All layers
        layers = new NeuralLayer[1 + hiddenLayers.length + 1];
        layers[0] = inputLayer;
        for (int i = 0; i < hiddenLayers.length; i++) {
            layers[i + 1] = hiddenLayers[i];
        }
        layers[layers.length - 1] = outputLayer;
    }

    public int getInputsCount() {
        return inputs.length;
    }

    public void setInputs(double[] inputs) {
        this.inputs = inputs;
    }

    public double[] getInputs() {
        return inputs;
    }

    public double getInput(int i) {
        return inputs[i];
    }

    public void calculate() {
        // Input layer
        inputLayer.setInputs(inputs);
        inputLayer.calculate();
        // Hidden layers
        for (NeuralHiddenLayer hiddenLayer : hiddenLayers) {
            hiddenLayer.setInputs(hiddenLayer.getPreviousLayer().getOutputs());
            hiddenLayer.calculate();
        }
        // Output layer
        outputLayer.setInputs(outputLayer.getPreviousLayer().getOutputs());
        outputLayer.calculate();
        outputs = outputLayer.getOutputs();
    }

    public int getOutputsCount() {
        return outputs.length;
    }

    public double[] getOutputs() {
        return outputs;
    }

    public double getOutput(int i) {
        return outputs[i];
    }

    public NeuralInputLayer getInputLayer() {
        return inputLayer;
    }

    public int getHiddenLayersCount() {
        return hiddenLayers.length;
    }

    public NeuralHiddenLayer[] getHiddenLayers() {
        return hiddenLayers;
    }

    public NeuralHiddenLayer getHiddenLayer(int i) {
        return hiddenLayers[i];
    }

    public NeuralOutputLayer getOutputLayer() {
        return outputLayer;
    }

    public int getLayersCount() {
        return layers.length;
    }

    public NeuralLayer[] getLayers() {
        return layers;
    }

    public NeuralLayer getLayer(int i) {
        return layers[i];
    }
}
