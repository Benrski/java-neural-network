package com.benrski.neuralnetwork;

import com.benrski.neuralnetwork.activationfunctions.IActivationFunction;
import com.benrski.neuralnetwork.activationfunctions.LinearFunction;
import com.benrski.neuralnetwork.initializers.IInitializer;

public class NeuralInputLayer extends NeuralLayer {

    public NeuralInputLayer(int inputsCount) {
        this(null, inputsCount, new LinearFunction());
    }

    public NeuralInputLayer(int inputsCount, IActivationFunction activationFunction) {
        this(null, inputsCount, activationFunction);
    }

    public NeuralInputLayer(NeuralNetwork network, int inputsCount, IActivationFunction activationFunction) {
        super(network, inputsCount, inputsCount, activationFunction);
        previousLayer = null;
    }

    @Override
    public void setPreviousLayer(NeuralLayer layer) {
        // Input layer can not have a previous layer
        previousLayer = null;
    }

    @Override
    public void setNextLayer(NeuralLayer layer) {
        // Setting next layer and checking for next's previous layer
        nextLayer = layer;
        if (layer.getPreviousLayer() != this) {
            layer.setPreviousLayer(this);
        }
    }

    @Override
    public void initialize(IInitializer initializer) {
        // Initializing input neurons
        // - 1 input for 1 neuron
        // - Bias and bias weight set to 0
        for (int i = 0; i < inputs.length; i++) {
            if (neurons[i] != null) {
                neurons[i].setActivationFunction(activationFunction);
                neurons[i].setLayer(this);
            } else {
                neurons[i] = new Neuron(this, 1, activationFunction);
            }
            neurons[i].setWeight(0, 1);
            neurons[i].setBias(0);
            neurons[i].setBiasWeight(0);
        }
    }

    @Override
    public void calculate() {
        // Calculating inputs (1 input for 1 neuron)
        for (int i = 0; i < neurons.length; i++) {
            double[] neuronInput = { inputs[i] };
            neurons[i].setInputs(neuronInput);
            neurons[i].calculate();
            outputs[i] = neurons[i].getOutput();
        }
    }
}
