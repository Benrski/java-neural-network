package com.benrski.neuralnetwork;

import com.benrski.neuralnetwork.activationfunctions.IActivationFunction;

public class NeuralOutputLayer extends NeuralLayer {

    public NeuralOutputLayer(int outputsCount, int inputsCount, IActivationFunction activationFunction) {
        this(null, outputsCount, inputsCount, activationFunction);
    }

    public NeuralOutputLayer(NeuralNetwork network, int outputsCount, int inputsCount,
            IActivationFunction activationFunction) {
        super(network, outputsCount, inputsCount, activationFunction);
        nextLayer = null;
    }

    @Override
    public void setPreviousLayer(NeuralLayer layer) {
        // Setting previous layer and checking for previous' next layer
        previousLayer = layer;
        if (layer.getNextLayer() != this) {
            layer.setNextLayer(this);
        }
    }

    @Override
    public void setNextLayer(NeuralLayer layer) {
        // Output layer can not have a next layer
        nextLayer = null;
    }
}
