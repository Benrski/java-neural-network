package com.benrski.neuralnetwork;

import com.benrski.neuralnetwork.activationfunctions.IActivationFunction;
import com.benrski.neuralnetwork.initializers.IInitializer;

public class NeuralLayer {
    // Neurons
    protected Neuron[] neurons = new Neuron[0];
    // Inputs
    protected double[] inputs = new double[0];
    // Ouputs
    protected double[] outputs = new double[0];
    // Activation function
    protected IActivationFunction activationFunction;
    // Previous layer
    protected NeuralLayer previousLayer;
    // Next layer
    protected NeuralLayer nextLayer;
    // Network
    protected NeuralNetwork network;

    public NeuralLayer(int neuronsCount, int inputsCount, IActivationFunction activationFunction) {
        this(null, neuronsCount, inputsCount, activationFunction);
    }

    public NeuralLayer(NeuralNetwork network, int neuronsCount, int inputsCount,
            IActivationFunction activationFunction) {
        this.network = network;
        this.neurons = new Neuron[neuronsCount];
        this.inputs = new double[inputsCount];
        this.outputs = new double[neuronsCount];
        this.activationFunction = activationFunction;
    }

    public void setNetwork(NeuralNetwork network) {
        this.network = network;
    }

    public NeuralNetwork getNetwork() {
        return network;
    }

    public void setNeurons(Neuron[] neurons) {
        this.neurons = neurons;
    }

    public Neuron[] getNeurons() {
        return neurons;
    }

    public void setNeuron(int i, Neuron neuron) {
        neurons[i] = neuron;
    }

    public Neuron getNeuron(int i) {
        return neurons[i];
    }

    public int getNeuronsCount() {
        return neurons.length;
    }

    public void setPreviousLayer(NeuralLayer layer) {
        previousLayer = layer;
    }

    public NeuralLayer getPreviousLayer() {
        return previousLayer;
    }

    public void setNextLayer(NeuralLayer layer) {
        nextLayer = layer;
    }

    public NeuralLayer getNextLayer() {
        return nextLayer;
    }

    public void initialize(IInitializer initializer) {
        for (int i = 0; i < neurons.length; i++) {
            if (neurons[i] != null) {
                neurons[i].setActivationFunction(activationFunction);
                neurons[i].setLayer(this);
            } else {
                neurons[i] = new Neuron(this, inputs.length, activationFunction);
            }
            neurons[i].initialize(initializer);
        }
    }

    public void calculate() {
        for (int i = 0; i < neurons.length; i++) {
            neurons[i].setInputs(inputs);
            neurons[i].calculate();
            outputs[i] = neurons[i].getOutput();
        }
    }

    public void setInputs(double[] inputs) {
        this.inputs = inputs;
    }

    public double[] getInputs() {
        return inputs;
    }

    public int getInputsCount() {
        return inputs.length;
    }

    public void setInput(int i, double input) {
        inputs[i] = input;
    }

    public double getInput(int i) {
        return inputs[i];
    }

    public double[] getOutputs() {
        return outputs;
    }

    public int getOutputsCount() {
        return outputs.length;
    }

    public double getOutput(int i) {
        return outputs[i];
    }

    public void setActivationFunction(IActivationFunction activationFunction) {
        this.activationFunction = activationFunction;
    }

    public IActivationFunction getActivationFunction() {
        return activationFunction;
    }
}
