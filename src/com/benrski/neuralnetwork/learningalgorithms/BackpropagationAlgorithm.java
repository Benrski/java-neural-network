package com.benrski.neuralnetwork.learningalgorithms;

import com.benrski.neuralnetwork.NeuralLayer;
import com.benrski.neuralnetwork.NeuralNetwork;
import com.benrski.neuralnetwork.Neuron;

public class BackpropagationAlgorithm extends AbstractLearningAlgorithm {
    protected double momentumRate;

    protected int trainingIndex;

    protected double[][] deltas;
    protected double[][][] weights;
    protected double[][][] deltaWeights;

    public BackpropagationAlgorithm(NeuralNetwork network) {
        this(network, new double[0][], new double[0][]);
    }

    public BackpropagationAlgorithm(NeuralNetwork network, double[][] trainingInputs, double[][] trainingOutputs) {
        super(network, trainingInputs, trainingOutputs);
        initializeDeltas();
        initializeWeights();
    }

    public void setMomentumRate(double momentumRate) {
        this.momentumRate = momentumRate;
    }

    public double getMomentumRate() {
        return momentumRate;
    }

    private void initializeDeltas() {
        int layersCount = network.getLayersCount() - 1;
        deltas = new double[layersCount][];
        for (int l = 0; l < layersCount; l++) {
            NeuralLayer layer = network.getLayer(l + 1);
            deltas[l] = new double[layer.getNeuronsCount()];
        }
    }

    private void initializeWeights() {
        int layersCount = network.getLayersCount() - 1;
        weights = new double[layersCount][][];
        deltaWeights = new double[layersCount][][];
        for (int l = 0; l < layersCount; l++) {
            NeuralLayer layer = network.getLayer(l + 1);
            int neuronsCount = layer.getNeuronsCount();
            weights[l] = new double[neuronsCount][];
            deltaWeights[l] = new double[neuronsCount][];
            for (int n = 0; n < neuronsCount; n++) {
                Neuron neuron = layer.getNeuron(n);
                int inputsCount = neuron.getInputsCount() + 1;
                weights[l][n] = new double[inputsCount];
                deltaWeights[l][n] = new double[inputsCount];
            }
        }
    }

    @Override
    public void train() {
        System.out.println("Training started...");

        synchronized (network) {
            forward();
        }

        trainingCycles = 0;
        trainingIndex = 0;

        while (trainingCycles < maximumTrainingCycles && error > minimumError) {
            synchronized (network) {
                forward(trainingIndex);
                print();
                backward();
            }
            trainingIndex++;
            if (trainingIndex >= trainingInputs.length) {
                trainingIndex = 0;
                trainingCycles++;
            }
        }

        System.out.println("Training completed!");
    }

    private double calculateDeltaWeight(int l, int n, int i) {
        NeuralLayer layer = network.getLayer(l + 1);
        Neuron neuron = layer.getNeuron(n);
        NeuralLayer nextLayer = layer.getNextLayer();
        double delta;
        if (nextLayer == null) {
            double deltaNeuron = trainingOutputs[trainingIndex][n] - networkOutputs[trainingIndex][n];
            delta = deltaNeuron * neuron.getDerivative();
        } else {
            double deltaNextLayer = 0;
            for (int nNext = 0; nNext < nextLayer.getNeuronsCount(); nNext++) {
                Neuron nextNeuron = nextLayer.getNeuron(nNext);
                deltaNextLayer += nextNeuron.getWeight(n) * deltas[l + 1][nNext];
            }
            delta = deltaNextLayer * neuron.getDerivative();
        }
        deltas[l][n] = delta;
        double input = i < neuron.getInputsCount() ? neuron.getInput(i) : neuron.getBias();
        return input * delta * learningRate;
    }

    private void backward() {
        int layersCount = network.getLayersCount() - 1;
        for (int l = (layersCount - 1); l >= 0; l--) {
            NeuralLayer layer = network.getLayer(l + 1);
            int neuronsCount = layer.getNeuronsCount();
            for (int n = 0; n < neuronsCount; n++) {
                Neuron neuron = layer.getNeuron(n);
                int inputsCount = neuron.getInputsCount();
                for (int i = 0; i <= inputsCount; i++) {
                    double currNewWeight = i < inputsCount ? neuron.getWeight(i) : neuron.getBiasWeight();
                    double deltaWeight = calculateDeltaWeight(l, n, i);
                    weights[l][n][i] = currNewWeight + deltaWeight;
                }
            }
        }
        applyWeights();
    }

    private void applyWeights() {
        int layersCount = network.getLayersCount() - 1;
        for (int l = 0; l < layersCount; l++) {
            NeuralLayer layer = network.getLayer(l + 1);
            double momentumSign = layer.getNextLayer() == null ? 1.0 : -1.0;
            int neuronsCount = layer.getNeuronsCount();
            for (int n = 0; n < neuronsCount; n++) {
                Neuron neuron = layer.getNeuron(n);
                int inputsCount = neuron.getInputsCount();
                for (int i = 0; i <= inputsCount; i++) {
                    double momentum = momentumSign * momentumRate * deltaWeights[l][n][i];
                    double newWeight = weights[l][n][i] + momentum;
                    double oldWeight = i < inputsCount ? neuron.getWeight(i) : neuron.getBiasWeight();
                    deltaWeights[l][n][i] = newWeight - oldWeight;
                    if (i < inputsCount) {
                        neuron.setWeight(i, newWeight);
                    } else {
                        neuron.setBiasWeight(newWeight);
                    }
                }
            }
        }
    }

    private void print() {
        System.out.println("Training cycle = " + (trainingCycles + 1) + "; Training set = " + (trainingIndex + 1)
                + "; Error = " + error);
    }
}
