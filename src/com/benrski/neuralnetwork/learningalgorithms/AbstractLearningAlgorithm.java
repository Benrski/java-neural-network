package com.benrski.neuralnetwork.learningalgorithms;

import com.benrski.neuralnetwork.NeuralNetwork;

public abstract class AbstractLearningAlgorithm implements ILearningAlgorithm {
    protected NeuralNetwork network;
    protected double learningRate;
    protected long trainingCycles;
    protected long maximumTrainingCycles;
    protected double error;
    protected double minimumError;
    protected double[][] trainingInputs;
    protected double[][] trainingOutputs;
    protected double[][] networkOutputs;

    public AbstractLearningAlgorithm(NeuralNetwork network) {
        this(network, new double[0][], new double[0][]);
    }

    public AbstractLearningAlgorithm(NeuralNetwork network, double[][] trainingInputs, double[][] trainingOutputs) {
        this.error = Double.MAX_VALUE;
        this.network = network;
        setTrainingData(trainingInputs, trainingOutputs);
    }

    public void setNetwork(NeuralNetwork network) {
        this.network = network;
    }

    public NeuralNetwork getNetwork() {
        return network;
    }

    public void setTrainingData(double[][] trainingInputs, double[][] trainingOutputs) {
        this.trainingInputs = trainingInputs;
        this.trainingOutputs = trainingOutputs;
        networkOutputs = new double[trainingOutputs.length][network.getOutputsCount()];
    }

    public double[][] getTrainingInputs() {
        return trainingInputs;
    }

    public double[][] getTrainingOutputs() {
        return trainingOutputs;
    }

    public void setLearningRate(double learningRate) {
        this.learningRate = learningRate;
    }

    public double getLearningRate() {
        return learningRate;
    }

    public void setMaximumTrainingCycles(long maximumTrainingCycles) {
        this.maximumTrainingCycles = maximumTrainingCycles;
    }

    public long getMaximumTrainingCycles() {
        return maximumTrainingCycles;
    }

    public void setMinimumError(double minimumError) {
        this.minimumError = minimumError;
    }

    public double getMinimumError() {
        return minimumError;
    }

    protected void calculateError() {
        int n = trainingOutputs.length;
        int nY = n > 0 ? trainingOutputs[0].length : 0;
        error = 0;
        for (int i = 0; i < n; i++) {
            double resultY = 0.0;
            for (int j = 0; j < nY; j++) {
                resultY += (trainingOutputs[i][j] - networkOutputs[i][j]);
            }
            error += Math.pow((1.0 / nY) * resultY, 2.0);
        }
        error *= (1.0 / n);
    }

    protected void forward() {
        for (int i = 0; i < trainingInputs.length; i++) {
            forward(i, false);
        }
        calculateError();
    }

    protected void forward(int i) {
        forward(i, true);
    }

    private void forward(int i, boolean calculateError) {
        network.setInputs(trainingInputs[i]);
        network.calculate();
        for (int j = 0; j < network.getOutputsCount(); j++) {
            networkOutputs[i][j] = network.getOutputs()[j];
        }
        if (calculateError) {
            calculateError();
        }
    }
}
