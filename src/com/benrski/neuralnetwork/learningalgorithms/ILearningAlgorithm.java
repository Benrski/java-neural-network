package com.benrski.neuralnetwork.learningalgorithms;

public interface ILearningAlgorithm {
    public void train();
}
