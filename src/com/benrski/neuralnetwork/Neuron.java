package com.benrski.neuralnetwork;

import com.benrski.neuralnetwork.activationfunctions.IActivationFunction;
import com.benrski.neuralnetwork.initializers.IInitializer;

public class Neuron {
    // Inputs
    private double[] inputs = new double[0];
    // Weights
    private double[] weights = new double[0];
    // Bias
    private double bias = 1;
    // Bias weight
    private double biasWeight;
    // Output
    private double output;
    // Derivative (output)
    private double derivative;
    // Activation function
    private IActivationFunction activationFunction;
    // Layer
    private NeuralLayer layer;

    public Neuron(int inputsCount, IActivationFunction activationFunction) {
        this(null, inputsCount, activationFunction);
    }

    public Neuron(NeuralLayer layer, int inputsCount, IActivationFunction activationFunction) {
        this.layer = layer;
        this.inputs = new double[inputsCount];
        this.weights = new double[inputsCount];
        this.activationFunction = activationFunction;
    }

    public int getInputsCount() {
        return inputs.length;
    }

    public void setInputs(double[] inputs) {
        this.inputs = inputs;
    }

    public double[] getInputs() {
        return inputs;
    }

    public void setInput(int i, double input) {
        inputs[i] = input;
    }

    public double getInput(int i) {
        return inputs[i];
    }

    public int getWeightsCount() {
        return weights.length;
    }

    public void setWeights(double[] weights) {
        this.weights = weights;
    }

    public double[] getWeights() {
        return weights;
    }

    public void setWeight(int i, double weight) {
        weights[i] = weight;
    }

    public double getWeight(int i) {
        return weights[i];
    }

    public void setBiasWeight(double biasWeight) {
        this.biasWeight = biasWeight;
    }

    public double getBiasWeight() {
        return biasWeight;
    }

    public void setBias(double bias) {
        this.bias = bias;
    }

    public double getBias() {
        return bias;
    }

    public void setActivationFunction(IActivationFunction activationFunction) {
        this.activationFunction = activationFunction;
    }

    public IActivationFunction getActivationFunction() {
        return activationFunction;
    }

    public void setLayer(NeuralLayer layer) {
        this.layer = layer;
    }

    public NeuralLayer getLayer() {
        return layer;
    }

    public double getOutput() {
        return output;
    }

    public double getDerivative() {
        return derivative;
    }

    public void initialize(IInitializer initializer) {
        initializer.initialize(this);
    }

    public void calculate() {
        double outputBeforeActivation = 0;
        for (int i = 0; i < inputs.length; i++) {
            outputBeforeActivation += (inputs[i] * weights[i]);
        }
        outputBeforeActivation += (bias * biasWeight);
        output = activationFunction.calculate(outputBeforeActivation);
        derivative = activationFunction.derivative(outputBeforeActivation);
    }
}
