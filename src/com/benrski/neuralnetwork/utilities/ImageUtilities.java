package com.benrski.neuralnetwork.utilities;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;

public class ImageUtilities {
    public static BufferedImage getResizedImage(BufferedImage image, int width, int height, Color background) {
        float sourceAspectRatio = (float) image.getWidth() / image.getHeight();
        float resultAspectRatio = (float) width / height;
        float newWidth, newHeight;
        if (resultAspectRatio > sourceAspectRatio) {
            newHeight = height;
            newWidth = (newHeight / image.getHeight()) * image.getWidth();
        } else {
            newWidth = width;
            newHeight = (newWidth / image.getWidth()) * image.getHeight();
        }
        Image scaledImage = image.getScaledInstance((int) newWidth, (int) newHeight, Image.SCALE_SMOOTH);
        BufferedImage resizedImage = new BufferedImage(width, height, image.getType());
        Graphics2D g2D = resizedImage.createGraphics();
        g2D.setColor(background);
        g2D.fillRect(0, 0, resizedImage.getWidth(), resizedImage.getHeight());
        g2D.drawImage(scaledImage, (int) (width / 2 - newWidth / 2), (int) (height / 2 - newHeight / 2), null);
        g2D.dispose();
        return resizedImage;
    }

    public static BufferedImage centerImageWithWeight(BufferedImage image, int width, int height, Color background) {
        BufferedImage result = new BufferedImage(width, height, image.getType());
        Graphics2D g2 = result.createGraphics();
        g2.setColor(background);
        g2.fillRect(0, 0, result.getWidth(), result.getHeight());
        long xWeight = 0;
        long yWeight = 0;
        long totalWeight = 0;
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                int weight = new Color(image.getRGB(x, y)).getRed();
                xWeight += x * weight;
                yWeight += y * weight;
                totalWeight += weight;
            }
        }
        int centerX = (int) ((double) xWeight / totalWeight);
        int centerY = (int) ((double) yWeight / totalWeight);
        g2.drawImage(image, width / 2 - centerX, height / 2 - centerY, null);
        g2.dispose();
        return result;
    }
}
