package com.benrski.neuralnetwork.utilities;

public class MathUtilities {
    public static double map(double n, double start1, double stop1, double start2, double stop2) {
        return map(n, start1, stop1, start2, stop2, true);
    }

    public static double map(double n, double start1, double stop1, double start2, double stop2, boolean withinBounds) {
        double newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
        if (!withinBounds) {
            return newval;
        }
        if (start2 < stop2) {
            return constrain(newval, start2, stop2);
        } else {
            return constrain(newval, stop2, start2);
        }
    }

    public static float map(float n, float start1, float stop1, float start2, float stop2) {
        return map(n, start1, stop1, start2, stop2, true);
    }

    public static float map(float n, float start1, float stop1, float start2, float stop2, boolean withinBounds) {
        float newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
        if (!withinBounds) {
            return newval;
        }
        if (start2 < stop2) {
            return constrain(newval, start2, stop2);
        } else {
            return constrain(newval, stop2, start2);
        }
    }

    public static int map(int n, int start1, int stop1, int start2, int stop2) {
        return map(n, start1, stop1, start2, stop2, true);
    }

    public static int map(int n, int start1, int stop1, int start2, int stop2, boolean withinBounds) {
        int newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
        if (!withinBounds) {
            return newval;
        }
        if (start2 < stop2) {
            return constrain(newval, start2, stop2);
        } else {
            return constrain(newval, stop2, start2);
        }
    }

    public static long map(long n, long start1, long stop1, long start2, long stop2) {
        return map(n, start1, stop1, start2, stop2, true);
    }

    public static long map(long n, long start1, long stop1, long start2, long stop2, boolean withinBounds) {
        long newval = (n - start1) / (stop1 - start1) * (stop2 - start2) + start2;
        if (!withinBounds) {
            return newval;
        }
        if (start2 < stop2) {
            return constrain(newval, start2, stop2);
        } else {
            return constrain(newval, stop2, start2);
        }
    }

    public static double constrain(double n, double low, double high) {
        return Math.max(Math.min(n, high), low);
    }

    public static float constrain(float n, float low, float high) {
        return Math.max(Math.min(n, high), low);
    }

    public static int constrain(int n, int low, int high) {
        return Math.max(Math.min(n, high), low);
    }

    public static long constrain(long n, long low, long high) {
        return Math.max(Math.min(n, high), low);
    }
}
