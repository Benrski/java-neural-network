package com.benrski.neuralnetwork.utilities;

import java.util.Random;

public class RandomGenerator {
    private static Random r = new Random();

    public static void setSeed(long seed) {
        r.setSeed(seed);
    }

    public static int nextInt() {
        return r.nextInt();
    }

    public static int nextInt(int bound) {
        return r.nextInt(bound);
    }

    public static int nextInt(int minBound, int maxBound) {
        if (maxBound <= minBound) {
            throw new IllegalArgumentException("maximum bound must be greater than minimum bound");
        }
        return r.nextInt(maxBound - minBound) + minBound;
    }

    public static double nextDouble() {
        return r.nextDouble();
    }

    public static double nextDouble(double bound) {
        if (bound <= 0) {
            throw new IllegalArgumentException("bound must be positive");
        }
        return nextDouble(0, bound);
    }

    public static double nextDouble(double minBound, double maxBound) {
        if (maxBound <= minBound) {
            throw new IllegalArgumentException("maximum bound must be greater than minimum bound");
        }
        return minBound + (r.nextDouble() * (maxBound - minBound));
    }

    public static double nextGaussian() {
        return r.nextGaussian();
    }

    public static double nextGaussian(double mean, double standardDeviation) {
        return mean + (r.nextGaussian() * standardDeviation);
    }
}
