package com.benrski.neuralnetwork.initializers;

import com.benrski.neuralnetwork.Neuron;
import com.benrski.neuralnetwork.utilities.RandomGenerator;

public class RandomNormalInitializer implements IInitializer {
    private double mean;
    private double standardDeviation;

    public RandomNormalInitializer() {
        this(0, 1);
    }

    public RandomNormalInitializer(double mean, double standardDeviation) {
        this.mean = mean;
        this.standardDeviation = standardDeviation;
    }

    private double generate() {
        return RandomGenerator.nextGaussian(mean, standardDeviation);
    }

    @Override
    public void initialize(Neuron neuron) {
        // Initializing weights
        for (int i = 0; i < neuron.getInputsCount(); i++) {
            neuron.setWeight(i, generate());
        }
        // Initializing bias weight
        neuron.setBiasWeight(generate());
    }
}
