package com.benrski.neuralnetwork.initializers;

import com.benrski.neuralnetwork.NeuralLayer;
import com.benrski.neuralnetwork.Neuron;
import com.benrski.neuralnetwork.utilities.RandomGenerator;

public class GlorotUniformInitializer implements IInitializer {
    private double generate(double range) {
        return RandomGenerator.nextDouble(-range, range);
    }

    @Override
    public void initialize(Neuron neuron) {
        // Calculating range
        int inputsCount = neuron.getInputsCount();
        int outputsCount = 1;
        NeuralLayer layer = neuron.getLayer();
        NeuralLayer nextLayer = layer.getNextLayer();
        if (layer != null && nextLayer != null) {
            outputsCount *= nextLayer.getNeuronsCount();
        }
        double range = Math.sqrt(6.0 / (inputsCount + outputsCount));
        // Initializing weights
        for (int i = 0; i < inputsCount; i++) {
            neuron.setWeight(i, generate(range));
        }
        // Initializing bias weight
        neuron.setBiasWeight(generate(range));
    }
}
