package com.benrski.neuralnetwork.initializers;

import com.benrski.neuralnetwork.Neuron;
import com.benrski.neuralnetwork.utilities.RandomGenerator;

public class KaimingUniformInitializer implements IInitializer {
    private double generate(double range) {
        return RandomGenerator.nextDouble(-range, range);
    }

    @Override
    public void initialize(Neuron neuron) {
        // Calculating range
        int inputsCount = neuron.getInputsCount();
        double range = Math.sqrt(2.0 / inputsCount);
        // Initializing weights
        for (int i = 0; i < inputsCount; i++) {
            neuron.setWeight(i, generate(range));
        }
        // Initializing bias weight
        neuron.setBiasWeight(generate(range));
    }
}
