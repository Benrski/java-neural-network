package com.benrski.neuralnetwork.initializers;

import com.benrski.neuralnetwork.Neuron;

public interface IInitializer {
    public void initialize(Neuron neuron);
}
