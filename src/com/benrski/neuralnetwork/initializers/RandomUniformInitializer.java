package com.benrski.neuralnetwork.initializers;

import com.benrski.neuralnetwork.Neuron;
import com.benrski.neuralnetwork.utilities.RandomGenerator;

public class RandomUniformInitializer implements IInitializer {
    private double minimum;
    private double maximum;

    public RandomUniformInitializer() {
        this(0, 1);
    }

    public RandomUniformInitializer(double range) {
        this(-range, range);
    }

    public RandomUniformInitializer(double minimum, double maximum) {
        this.minimum = minimum;
        this.maximum = maximum;
    }

    private double generate() {
        return RandomGenerator.nextDouble(minimum, maximum);
    }

    @Override
    public void initialize(Neuron neuron) {
        // Initializing weights
        for (int i = 0; i < neuron.getInputsCount(); i++) {
            neuron.setWeight(i, generate());
        }
        // Initializing bias weight
        neuron.setBiasWeight(generate());
    }
}
