package com.benrski.neuralnetwork.initializers;

import com.benrski.neuralnetwork.Neuron;
import com.benrski.neuralnetwork.utilities.RandomGenerator;

public class KaimingNormalInitializer implements IInitializer {
    private double mean;

    public KaimingNormalInitializer() {
        this(0);
    }

    public KaimingNormalInitializer(double mean) {
        this.mean = mean;
    }

    private double generate(double standardDeviation) {
        return RandomGenerator.nextGaussian(mean, standardDeviation);
    }

    @Override
    public void initialize(Neuron neuron) {
        // Calculating standard deviation
        int inputsCount = neuron.getInputsCount();
        double standardDeviation = Math.sqrt(2.0 / inputsCount);
        // Initializing weights
        for (int i = 0; i < inputsCount; i++) {
            neuron.setWeight(i, generate(standardDeviation));
        }
        // Initializing bias weight
        neuron.setBiasWeight(generate(standardDeviation));
    }
}
