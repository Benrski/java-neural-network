package com.benrski.neuralnetwork.initializers;

import com.benrski.neuralnetwork.NeuralLayer;
import com.benrski.neuralnetwork.Neuron;
import com.benrski.neuralnetwork.utilities.RandomGenerator;

public class GlorotNormalInitializer implements IInitializer {
    private double mean;

    public GlorotNormalInitializer() {
        this(0);
    }

    public GlorotNormalInitializer(double mean) {
        this.mean = mean;
    }

    private double generate(double standardDeviation) {
        return RandomGenerator.nextGaussian(mean, standardDeviation);
    }

    @Override
    public void initialize(Neuron neuron) {
        // Calculating standard deviation
        int inputsCount = neuron.getInputsCount();
        int outputsCount = 1;
        NeuralLayer layer = neuron.getLayer();
        NeuralLayer nextLayer = layer.getNextLayer();
        if (layer != null && nextLayer != null) {
            outputsCount *= nextLayer.getNeuronsCount();
        }
        double standardDeviation = Math.sqrt(2.0 / (inputsCount + outputsCount));
        // Initializing weights
        for (int i = 0; i < inputsCount; i++) {
            neuron.setWeight(i, generate(standardDeviation));
        }
        // Initializing bias weight
        neuron.setBiasWeight(generate(standardDeviation));
    }
}
